const accordionItems = document.querySelectorAll('.m-accordion__item');

accordionItems.forEach(el => el.addEventListener('click', () => {
	if(el.classList.contains('open')){
		el.classList.remove('open');
	} else {
		accordionItems.forEach(el2 => el2.classList.remove('open'));
		el.classList.add('open');
	}
}));
