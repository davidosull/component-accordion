# Component - Accordion

- Add/Remove .open class to show and hide first tab on page load.
- Transitions and transition delays are not using vars to allow for the animation of accordion content max-height.
- Remove the following in production:
```
  .m-accordion {
  margin: 0 auto;
  max-width: 40rem;
  }
```
